Aplicació feta per:
    
    · PAU ALOS MAIRAL
    · GERARD ESTEVE VILLUENDAS
    · ORIOL SABORIDO ORÚS
    · MIGUEL ANGEL ALVAREZ VAZQUEZ
    
Link a la app a heroku:

https://issue-traker-client.herokuapp.com/

Link al repositori del servidor:

https://bitbucket.org/paualos3/isuea_traker_asw


     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 

